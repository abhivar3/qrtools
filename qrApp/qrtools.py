import pyqrcode 
from pyqrcode import QRCode 
import png
  
def createqr(s):
    # String which represent the QR code 
        
    # Generate QR code 
    url = pyqrcode.create(s) 
    
    # Create and save the png file naming "myqr.png" 
    file_name = s + ".png"
    url.png(file_name , scale = 8) 

